from django.db import models


class Musician(models.Model):
    class Meta(object):
        ordering = ["name"]

    name = models.CharField(max_length=100)
    instrument = models.CharField(max_length=100)


class Album(models.Model):
    class Meta(object):
        # `class Meta` es exactamente igual a `class Meta(object)`.
        # Decidirse por uno, verlo siempre de la misma forma reduce la carga cognitiva,
        # menos dudas. Código más uniforme.
        # El código se escribe una vez y se lee muchas
        ordering = ["num_stars"]

    name = models.CharField(max_length=100)
    release_date = models.DateField()
    num_stars = models.IntegerField()
    artist = models.ForeignKey(Musician, on_delete=models.CASCADE)


# Hay que distinguir cuando algo es un bad smell de cuando es una decisión estilística
# En este caso da igual que decidamos poner las FK arriba o abajo, pero es mejor decidir
# algo que se pongan donde cuadre, arriba, abajo, por el medio, ...
# Se escoge la posición por defecto del plugin `flake8-class-attributes-order` porqué
# andar modificando los valores por defecto de los plugins implican más mantenimiento
