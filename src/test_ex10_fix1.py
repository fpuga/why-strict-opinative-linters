import unittest
from numbers import Number
from typing import Union


SoldProduct = dict[str, Union[Number, str]]

# Un par de Extract Method dejan el código bastante mejor pero seguimos viendo problemas
# a estas alturas incluyendo el complejo tipo que tenemos de SoldProduct, el "naming"
# que estamos usando, y los "switch", usar clases debería ser una de las opciones
# Los WPS226 que nos quedan son causa de tener los tests en el mismo módulo si los
# comentamos ya se eliminarían.
# Así que los linters no son la solución final, podemos eliminar los warnings y aún así
# quedan bad smells en el código, pero al menos dan pie a visibilizar problemas


def get_prize_per_unit(item: SoldProduct) -> Number:
    product = item["product"]
    if product == "oranges":
        unit_prize = 2
    if product == "apples":
        unit_prize = 3
    if product == "pears":
        unit_prize = 6
    if product == "bananas":
        unit_prize = 10

    return unit_prize


def get_cost_per_unit(item: SoldProduct) -> Number:
    product = item["product"]
    if product == "oranges":
        unit_cost = 1
    if product == "apples":
        unit_cost = 2
    if product == "pears":
        unit_cost = 3
    if product == "bananas":
        unit_cost = 4
    return unit_cost


def calculate_prize(item: SoldProduct):
    unit_prize = get_prize_per_unit(item)
    return item["quantity"] * unit_prize * item.get("discount", 1)


def calculate_cost(item: SoldProduct):
    unit_cost = get_cost_per_unit(item)
    return item["quantity"] * unit_cost


def calculate_revenue(item: SoldProduct):
    return calculate_prize(item) - calculate_cost(item)


def calculate_item_with_bigger_price(
    sold_products: list[SoldProduct],
) -> dict[str, Union[Number, str]]:
    bigger_prize = 0
    item_with_bigger_prize = None
    for item in sold_products:
        prize = calculate_prize(item)
        if prize > bigger_prize:
            bigger_prize = prize
            item_with_bigger_prize = item.copy()

    return item_with_bigger_prize


def calculate_bigger_price(
    sold_products: list[SoldProduct],
) -> dict[str, Union[Number, str]]:
    bigger_prize = 0
    for item in sold_products:
        prize = calculate_prize(item)
        if prize > bigger_prize:
            bigger_prize = prize

    return bigger_prize


def calculate_item_with_bigger_revenue(sold_products: list[SoldProduct]) -> Number:
    bigger_revenue = 0
    item_with_bigger_revenue = None
    for item in sold_products:
        revenue = calculate_revenue(item)
        if revenue > bigger_revenue:
            bigger_revenue = revenue
            item_with_bigger_revenue = item.copy()

    return item_with_bigger_revenue


def calculate_bigger_revenue(sold_products: list[SoldProduct]) -> Number:
    bigger_revenue = 0
    for item in sold_products:
        revenue = calculate_revenue(item)
        if revenue > bigger_revenue:
            bigger_revenue = revenue

    return bigger_revenue


class TestCalculatePrice(unittest.TestCase):
    def setUp(self) -> None:
        self.sold_products = [
            {"client": "Alicia", "product": "oranges", "quantity": 4, "discount": 0.75},
            {"client": "Brais", "product": "apples", "quantity": 10},
            {
                "client": "Carlos",
                "product": "bananas",  # Que pasa si escribimos "banana" en lugar de "bananas"
                "quantity": 3,
                "discount": 0.8,
            },
        ]

    def test_calculate_item_with_bigger_price(self):
        result = calculate_item_with_bigger_price(self.sold_products)
        self.assertEquals(
            result, {"client": "Brais", "product": "apples", "quantity": 10}
        )

    def test_calculate_item_with_bigger_revenue(self):
        result = calculate_item_with_bigger_revenue(self.sold_products)

        self.assertEquals(
            result,
            {"client": "Carlos", "product": "bananas", "quantity": 3, "discount": 0.8},
        )

    def test_calculate_bigger_price(self):
        result = calculate_bigger_price(self.sold_products)
        self.assertAlmostEquals(result, 30)

    def test_calculate_bigger_revenue(self):
        # que pasa si necesitamos otro método para obtener sólo la mayor cantidad
        result = calculate_bigger_revenue(self.sold_products)
        self.assertAlmostEquals(result, 12)


if __name__ == "__main__":
    unittest.main()
