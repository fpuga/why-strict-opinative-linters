from django.db import models


class Musician(models.Model):
    class Meta(object):
        ordering = ["name"]

    name = models.CharField(max_length=100)
    instrument = models.CharField(max_length=100)


class Album(models.Model):
    artist = models.ForeignKey(Musician, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    release_date = models.DateField()
    num_stars = models.IntegerField()

    class Meta:
        ordering = ["num_stars"]


# Cual es la diferencia entre `class Meta` y `class Meta(object)`
# Que criterio seguimos para ordenar los "campos" en las clases
