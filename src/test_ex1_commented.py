import unittest
from numbers import Number


# No hacen falta comentarios

UNIT_PRIZE = 5
HIGH_VOLUME_DISCOUNT = 0.9


def calculate_price(quantity: int) -> Number:

    result: Number = quantity * UNIT_PRIZE
    if quantity > 10:
        result *= HIGH_VOLUME_DISCOUNT

    return result


class TestCalculatePrice(unittest.TestCase):
    def test_calculate_price(self):
        self.assertAlmostEquals(calculate_price(5), 25)
        self.assertAlmostEquals(calculate_price(11), 49.5)


if __name__ == "__main__":
    unittest.main()
