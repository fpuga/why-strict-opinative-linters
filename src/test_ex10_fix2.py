import unittest
from numbers import Number
from typing import Union


SoldProduct = dict[str, Union[Number, str]]

# Mejor. Aunque mejorable.


class Item(object):
    unit_prize = 0
    unit_cost = 0

    def __init__(self, d):
        self.org_dict = d.copy()
        self.product = d["product"]
        self.quantity = d["quantity"]
        self.discount = d.get("discount", 1)
        self.client = d["client"]

    @property
    def prize(self):
        return self.quantity * self.unit_prize * self.discount

    @property
    def revenue(self):
        cost = self.quantity * self.unit_cost
        return self.prize - cost

    def as_json(self):
        return self.org_dict


class Orange(Item):
    unit_prize = 2
    unit_cost = 1


class Apple(Item):
    unit_prize = 3
    unit_cost = 2


class Pear(Item):
    unit_prize = 6
    unit_cost = 3


class Banana(Item):
    unit_prize = 10
    unit_cost = 4


class ItemList(object):
    def __init__(self, items) -> None:
        self.items = items

    def find_item_with_bigger(self, quality: str) -> Item:
        return max(self.items, key=lambda x: getattr(x, quality))


def item_list_factory(sold_products: list[SoldProduct]) -> ItemList:
    return ItemList([item_factory(d) for d in sold_products])


def item_factory(d: SoldProduct) -> Item:
    class_finder = {
        "oranges": Orange,
        "apples": Apple,
        "pears": Pear,
        "bananas": Banana,
    }
    return class_finder[d["product"]](d)


def calculate_item_with_bigger_price(sold_products: list[SoldProduct]) -> SoldProduct:
    items = item_list_factory(sold_products)
    return items.find_item_with_bigger("prize").as_json()


def calculate_bigger_price(sold_products: list[SoldProduct]) -> Number:
    items = item_list_factory(sold_products)
    return items.find_item_with_bigger("prize").prize


def calculate_item_with_bigger_revenue(sold_products: list[SoldProduct]) -> Number:
    items = item_list_factory(sold_products)
    return items.find_item_with_bigger("revenue").as_json()


def calculate_bigger_revenue(sold_products: list[SoldProduct]) -> Number:
    items = item_list_factory(sold_products)
    return items.find_item_with_bigger("revenue").revenue


class TestCalculatePrice(unittest.TestCase):
    def setUp(self) -> None:
        self.sold_products = [
            {"client": "Alicia", "product": "oranges", "quantity": 4, "discount": 0.75},
            {"client": "Brais", "product": "apples", "quantity": 10},
            {
                "client": "Carlos",
                "product": "bananas",  # Que pasa si escribimos "banana" en lugar de "bananas"
                "quantity": 3,
                "discount": 0.8,
            },
        ]

    def test_calculate_item_with_bigger_price(self):
        result = calculate_item_with_bigger_price(self.sold_products)
        self.assertEquals(
            result, {"client": "Brais", "product": "apples", "quantity": 10}
        )

    def test_calculate_item_with_bigger_revenue(self):
        result = calculate_item_with_bigger_revenue(self.sold_products)

        self.assertEquals(
            result,
            {"client": "Carlos", "product": "bananas", "quantity": 3, "discount": 0.8},
        )

    def test_calculate_bigger_price(self):
        result = calculate_bigger_price(self.sold_products)
        self.assertAlmostEquals(result, 30)

    def test_calculate_bigger_revenue(self):
        # que pasa si necesitamos otro método para obtener sólo la mayor cantidad
        result = calculate_bigger_revenue(self.sold_products)
        self.assertAlmostEquals(result, 12)


if __name__ == "__main__":
    unittest.main()
