import unittest
from numbers import Number


def calculate_prize(item):
    return item["quantity"] * item["unit_prize"] * item.get("discount_coupon", 1)


def calculate_total_price(cart: list[dict]) -> Number:
    items_prizes = [calculate_prize(item) for item in cart]
    return sum(items_prizes)


class TestCalculateTotalPrice(unittest.TestCase):
    def test_calculate_total_price(self):
        cart = [
            {"quantity": 1, "unit_prize": 2, "discount_coupon": 0.9},
            {"quantity": 3, "unit_prize": 3.5},
        ]
        self.assertAlmostEquals(calculate_total_price(cart), 12.3)


if __name__ == "__main__":
    unittest.main()
