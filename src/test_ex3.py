import unittest
from numbers import Number


def calculate_prize(item):
    if "discount_coupon" in item:
        discount = item["discount_coupon"]
    else:
        discount = 1

    return item["quantity"] * item["unit_prize"] * discount


def calculate_total_price(cart: list[dict]) -> Number:
    total_prize = 0
    for item in cart:
        total_prize += calculate_prize(item)

    return total_prize


class TestCalculateTotalPrice(unittest.TestCase):
    def test_calculate_total_price(self):
        cart = [
            {"quantity": 1, "unit_prize": 2, "discount_coupon": 0.9},
            {"quantity": 3, "unit_prize": 3.5},
        ]
        self.assertAlmostEquals(calculate_total_price(cart), 12.3)


if __name__ == "__main__":
    unittest.main()
