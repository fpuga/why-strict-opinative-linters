import unittest
from numbers import Number
from typing import Union


# Este código tiene claramente "bad smells", pero tampoco es terrible. El echo de que
# el linter pite en un montón de sitios y no nos deje hacer commit, al final es una
# ayuda a no dejarlo así, a preocuparse de dejarlo mejor y enseguida vemos que tenemos
# opciones para satisfacer al linter, como usar constantes para los strings (WPS226) o
# alias para los tipos complicados (WPS234), pero a poco que valoremos enseguida vemos
# que WPS210 y WPS204 necesitan refactoring.


def get_prize_per_unit(item: dict[str, Union[Number, str]]) -> Number:
    if item["product"] == "oranges":
        unit_prize = 2
    if item["product"] == "apples":
        unit_prize = 3
    if item["product"] == "pears":
        unit_prize = 6
    if item["product"] == "bananas":
        unit_prize = 10

    return unit_prize


def get_cost_per_unit(item: dict[str, Union[Number, str]]) -> Number:
    if item["product"] == "oranges":
        unit_cost = 1
    if item["product"] == "apples":
        unit_cost = 2
    if item["product"] == "pears":
        unit_cost = 3
    if item["product"] == "bananas":
        unit_cost = 4
    return unit_cost


def calculate_item_with_bigger_price(
    sold_products: list[dict[str, Union[Number, str]]]
) -> dict[str, Union[Number, str]]:
    bigger_prize = 0
    item_with_bigger_prize = None
    for item in sold_products:
        unit_prize = get_prize_per_unit(item)
        prize = item["quantity"] * unit_prize * item.get("discount", 1)
        if prize > bigger_prize:
            bigger_prize = prize
            item_with_bigger_prize = item.copy()

    return item_with_bigger_prize


def calculate_bigger_price(
    sold_products: list[dict[str, Union[Number, str]]]
) -> dict[str, Union[Number, str]]:
    bigger_prize = 0
    for item in sold_products:
        unit_prize = get_prize_per_unit(item)
        prize = item["quantity"] * unit_prize * item.get("discount", 1)
        if prize > bigger_prize:
            bigger_prize = prize

    return bigger_prize


def calculate_item_with_bigger_revenue(
    sold_products: list[dict[str, Union[Number, str]]]
) -> Number:
    bigger_revenue = 0
    item_with_bigger_revenue = None
    for item in sold_products:
        unit_cost = get_cost_per_unit(item)
        unit_prize = get_prize_per_unit(item)
        prize = item["quantity"] * unit_prize * item.get("discount", 1)
        cost = item["quantity"] * unit_cost
        revenue = prize - cost
        if revenue > bigger_revenue:
            bigger_revenue = revenue
            item_with_bigger_revenue = item.copy()

    return item_with_bigger_revenue


def calculate_bigger_revenue(
    sold_products: list[dict[str, Union[Number, str]]]
) -> Number:
    bigger_revenue = 0
    for item in sold_products:
        unit_cost = get_cost_per_unit(item)
        unit_prize = get_prize_per_unit(item)
        prize = item["quantity"] * unit_prize * item.get("discount", 1)
        cost = item["quantity"] * unit_cost
        revenue = prize - cost
        if revenue > bigger_revenue:
            bigger_revenue = revenue

    return bigger_revenue


class TestCalculatePrice(unittest.TestCase):
    def setUp(self) -> None:
        self.sold_products = [
            {"client": "Alicia", "product": "oranges", "quantity": 4, "discount": 0.75},
            {"client": "Brais", "product": "apples", "quantity": 10},
            {
                "client": "Carlos",
                "product": "bananas",  # Que pasa si escribimos "banana" en lugar de "bananas"
                "quantity": 3,
                "discount": 0.8,
            },
        ]

    def test_calculate_item_with_bigger_price(self):
        result = calculate_item_with_bigger_price(self.sold_products)
        self.assertEquals(
            result, {"client": "Brais", "product": "apples", "quantity": 10}
        )

    def test_calculate_item_with_bigger_revenue(self):
        result = calculate_item_with_bigger_revenue(self.sold_products)

        self.assertEquals(
            result,
            {"client": "Carlos", "product": "bananas", "quantity": 3, "discount": 0.8},
        )

    def test_calculate_bigger_price(self):
        result = calculate_bigger_price(self.sold_products)
        self.assertAlmostEquals(result, 30)

    def test_calculate_bigger_revenue(self):
        # que pasa si necesitamos otro método para obtener sólo la mayor cantidad
        result = calculate_bigger_revenue(self.sold_products)
        self.assertAlmostEquals(result, 12)


if __name__ == "__main__":
    unittest.main()
