import sqlite3
import unittest


class MyException(Exception):
    """Base class for my project custom exceptions."""


def clients_that_by_more_oranges_than(value, con):
    """Obtiene de la tabla `oranges` los clientes que han comprado más de `value`."""
    cur = con.cursor()
    sql = f"SELECT client, quantity FROM orange WHERE quantity > {value}"
    cur.execute(sql)
    return cur.fetchall()


def clients_that_buy(user_input, con):
    """El cliente define de que tabla quiere obtener los datos y se los devolvemos.

    No es un ejemplo muy bueno pero se entiende la idea.
    """
    valid_tables = {"Apples": "apple", "Oranges": "orange"}
    table = valid_tables.get(user_input)
    if not table:
        raise MyException("Invalid Input")
    cur = con.cursor()
    sql = f"SELECT client, quantity FROM {table}"
    cur.execute(sql)
    return cur.fetchall()


class TestCalculateTotalPrice(unittest.TestCase):
    def setUp(self) -> None:
        self.con = sqlite3.connect(":memory:")
        cur = self.con.cursor()
        cur.execute("CREATE TABLE orange (client text, quantity integer)")
        cur.execute("INSERT INTO orange VALUES ('Alicia', 1), ('Brais', 6)")
        cur.execute("CREATE TABLE apple (client text, quantity integer)")
        cur.execute("INSERT INTO apple VALUES ('Carlos', 3), ('Daniela', 6)")
        self.con.commit()

    def tearDown(self) -> None:
        self.con.close()

    def test_user_value_filter(self):
        # Simulates a value introduced by the user to filter inputs bigger that a
        # quantity
        user_input_filter_big_that = 5
        result = clients_that_by_more_oranges_than(user_input_filter_big_that, self.con)
        self.assertEquals(len(result), 1)
        self.assertEquals(result[0], ("Brais", 6))

    def test_user_selects_table(self):
        # Simula que el usuario escoge la tabla a usar para la consulta en una
        # interfaz
        user_input_table_selected = "Apples"
        result = clients_that_buy(user_input_table_selected, self.con)
        self.assertEquals(len(result), 2)
        self.assertEquals(result[0], ("Carlos", 3))
        self.assertEquals(result[1], ("Daniela", 6))

    def test_user_selects_invalid_table(self):
        user_input_table_selected = "apple; DELETE FROM apple;"
        self.assertRaises(
            MyException, clients_that_buy, user_input_table_selected, self.con
        )


if __name__ == "__main__":
    unittest.main()
