# Why Strict Opinative Linters

Este repositorio contiene ejemplos de porqué me gustan los linters estrictos y opininativos.

En estos dos enlaces algunas generalidades y en el segundo con algún ejemplo:

* https://gitlab.com/icarto/ikdb/-/blob/wip_linters/linters_estilo_codigo_y_formatters/linters/1.introduccion.md
* https://gitlab.com/icarto/ikdb/-/blob/wip_linters/linters_estilo_codigo_y_formatters/linters/4.linters_python.md#una-justificaci%C3%B3n-sobre-el-uso-de-linters-estrictos-y-opinativos

Si pensamos porqué un linter estricto opinativo:

* Errores de sintaxis. Siempre útil, en cualquier contexto (desarrollo, pre-commit, ...). No tiene mucha discusión. test_ex1.py
* Carga cognitiva. Código uniforme. Cuanto menos haya que pensar y más uniforme sea el código mejor. Los formmatters ayudan. Un linter estricto opinitativo también. test_ex2.py
* Prácticas poco finas para el lenguaje, dioms. , ... No son necesariamente errores, si no práctias que en un determinado lenguaje están mal consideradas. test_ex3.py
* Malas prácticas. concatenar strings para un sql. test_ex4.py